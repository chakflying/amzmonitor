import requests
import shadow_useragent
import random
import math
import smtplib
import time
import webbrowser
import re
import datetime
import webbrowser
from bs4 import BeautifulSoup
from playsound import playsound
from urllib.parse import urlparse

URL = "https://www.amazon.co.jp/s?i=hpc&rh=n%3A160384011%2Cn%3A161669011%2Cn%3A169911011%2Cn%3A169922011%2Cp_76%3A2227293051%2Cp_6%3AAN1VRQENFRJN5&s=price-asc-rank"
USER_AGENT_LIST = [
    # Chrome
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    # Firefox
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0',
    'Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1'
]

def check_mask_price(m_url, m_UA):
    print(datetime.datetime.now())

    netloc = urlparse(m_url).netloc
    headers = {"User-Agent": m_UA}
    page = requests.get(m_url, headers=headers)
    soup = BeautifulSoup(page.content, 'html.parser')

    items = soup.find_all(
        'span', class_='a-size-base-plus a-color-base a-text-normal')
    selected_mask = ['いろはism おめかしマスク 1枚入 おひさま XM280-XM05',
                     'マスクシェル 鼻挿入型 レギュラーサイズ 3個入 MSN-R']

    for item in items:
        title_text = item.get_text()
        if title_text in selected_mask:
            print(f'Found: {title_text}')
            if item.parent.name == 'a':
                path = item.parent.get('href')
                print(f'https://{netloc}{path}')
                # webbrowser.open(f'https://{netloc}{path}', new=2)
            else:
                print("No link found.")
        else:
            print(f'No match: {title_text}')


if __name__ == "__main__":
    check_mask_price(URL, random.choice(USER_AGENT_LIST))
